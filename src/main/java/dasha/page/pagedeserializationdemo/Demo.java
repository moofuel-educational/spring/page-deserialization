package dasha.page.pagedeserializationdemo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Дмитрий
 * @since 30.01.2018
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Demo {

    private String kek;
    private Integer lol;
    private List<String> chebureki;
}
