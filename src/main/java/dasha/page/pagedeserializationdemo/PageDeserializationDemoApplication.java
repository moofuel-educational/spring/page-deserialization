package dasha.page.pagedeserializationdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PageDeserializationDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PageDeserializationDemoApplication.class, args);
    }
}
