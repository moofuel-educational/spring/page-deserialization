package dasha.page.pagedeserializationdemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static dasha.page.pagedeserializationdemo.PagedController.DEMO_LIST;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Дмитрий
 * @since 30.01.2018
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PagedControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void getDemoPage() {
        final ResponseEntity<RestResponsePage<Demo>> response = testRestTemplate.exchange(
                "/pages",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<RestResponsePage<Demo>>() {
                });
        final HttpStatus statusCode = response.getStatusCode();
        assertThat(statusCode.is2xxSuccessful())
                .isTrue();
        final PageImpl<Demo> actualBody = response.getBody();
        assertThat(actualBody.getTotalElements())
                .isEqualTo(6);
        assertThat(actualBody.getContent())
                .containsOnlyElementsOf(DEMO_LIST);
    }
}