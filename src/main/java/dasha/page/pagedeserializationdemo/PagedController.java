package dasha.page.pagedeserializationdemo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * @author Дмитрий
 * @since 30.01.2018
 */
@RestController
@RequestMapping("/pages")
public class PagedController {


    private static final String CHEBUREK_1 = "cheburek1";
    private static final String CHEBUREK_2 = "cheburek2";
    private static final String CHEBUREK_3 = "cheburek3";
    static final List<Demo> DEMO_LIST = Arrays.asList(
            new Demo("kek", 1337, Arrays.asList(CHEBUREK_1, CHEBUREK_2, CHEBUREK_3)),
            new Demo("kek", 1337, Arrays.asList(CHEBUREK_1, CHEBUREK_2, CHEBUREK_3)),
            new Demo("kek", 1337, Arrays.asList(CHEBUREK_1, CHEBUREK_2, CHEBUREK_3)),
            new Demo("kek", 1337, Arrays.asList(CHEBUREK_1, CHEBUREK_2, CHEBUREK_3)),
            new Demo("kek", 1337, Arrays.asList(CHEBUREK_1, CHEBUREK_2, CHEBUREK_3)),
            new Demo("kek", 1337, Arrays.asList(CHEBUREK_1, CHEBUREK_2, CHEBUREK_3)));

    @GetMapping
    public Page<Demo> getDemoPage() {
        return new PageImpl<>(DEMO_LIST);
    }
}
